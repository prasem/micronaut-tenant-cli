package com.staravito

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Produces
import io.micronaut.http.multipart.CompletedFileUpload


@Controller("/tenant")
class TenantLifeCycleController {

    @Post("/{env}/{region}", consumes = [MediaType.MULTIPART_FORM_DATA])
    @Produces(MediaType.TEXT_PLAIN)
    fun addTenant(
        file: CompletedFileUpload,
        env: ENV,
        region: REGION,
        internalName: String,
        name: String,
        platformName: String,
        subDomainName: String
    ): String {
        return "Tenant added successfully"
    }

    @Delete("/{env}/{region}/{tenantId}")
    @Produces(MediaType.TEXT_PLAIN)
    fun deleteTenant(
        env: ENV, region: REGION, tenantId: String, customerName: String
    ): String {
        return "Tenant deleted successfully"
    }
}

enum class ENV {
    PROD,
    STAGE,
    DEV
}

enum class REGION {
    US,
    EU
}