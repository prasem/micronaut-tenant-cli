package com.staravito

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces


@Controller("/health")
class HealthCheckController {

    @Get
    @Produces(MediaType.TEXT_PLAIN)
    fun index(): String {
        return "Hello I am healthy and ready to take orders"
    }
}