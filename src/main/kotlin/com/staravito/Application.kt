package com.staravito

import io.micronaut.runtime.Micronaut.*
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.info.License

@OpenAPIDefinition(
	info = Info(
		title = "micronaut-tenant-cli",
		version = "0.1",
		description = "Micronaut based application for adding and removing tenants",
	)
)
object Api {
}
fun main(args: Array<String>) {
	build()
		.args(*args)
		.packages("com.stravito")
		.start()
}

