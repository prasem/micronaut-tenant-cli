package com.staravito

import io.micronaut.http.HttpRequest
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import jakarta.inject.Inject

@MicronautTest
class HealthCheckControllerTest {

    @Inject
    @field:Client("/")
    lateinit var client : HttpClient

    @Test
    fun testHealth() {
//        val request: HttpRequest<Any> = HttpRequest.GET("/health")
//        val body = client.toBlocking().retrieve(request)
//        assertNotNull(body)
//        assertEquals("Hello I am healthy and ready to take orders", body)
    }
}
